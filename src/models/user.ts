export class User {
    public uid: string;
    public displayName: string;
    public phoneNumber: string;
    public birthday: Date;
    public email: string;
    public status: string;
    public instagram: string;
    public twitter: string;
    public snapchat: string;
}