import { Component } from '@angular/core';
import { NavParams, ViewController, LoadingController } from 'ionic-angular';

import { Alert } from '../../providers/alert';
import { DataProvider } from '../../providers/data-provider';

//Models
import {User} from '../../models/user';

@Component({
  selector: 'profile-edit-modal',
  templateUrl: 'profileEditModal.html'
})
export class ProfileEditModal {

  public user = {} as User;
  public mockUser = {} as User;
  public loading;

  constructor(
    private params: NavParams, 
    private viewCtrl: ViewController, 
    private alert: Alert,
    private dataProvider: DataProvider,
    private loadingCtrl: LoadingController) {

    this.loading = this.loadingCtrl.create({content: "Enviando dados"});
    this.user = params.get('user');   //  getting user passed form profile Modal Controller
    this.mockUser = Object.assign({} as User, this.user); // copying to a temporary object so we can make changes
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  update(){
    this.loading.present();
    //  making mockUser more optimized for update by
    //  taking out fields that don't change
    for (var key in this.mockUser) {
      if(this.mockUser[key] === this.user[key] || this.mockUser[key] === undefined){
        delete this.mockUser[key];
      }
    }

    this.dataProvider.update(`users/${this.user.uid}`, this.mockUser)
      .then(res => {
        Object.assign(this.user, this.mockUser);  // changing object sent via NavParams, so we can see the changes
        this.loading.dismiss();
        this.viewCtrl.dismiss();
      });
  }


}
