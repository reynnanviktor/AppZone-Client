import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
//providers
import {Alert} from '../../providers/alert';
import {DataProvider} from '../../providers/data-provider';
import {AuthProvider} from '../../providers/auth-provider';
//  pages
import { HomePage } from '../home/home';
/*
  Generated class for the DetailsSignUp page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-details-sign-up',
  templateUrl: 'details-sign-up.html'
})
export class DetailsSignUpPage {
  //  Fields
  public email: string;
  public password: string;
  public name: string;
  public lastname: string;
  public phone: string;
  public birthday: Date;

  private loggedUser: firebase.User;

  constructor(
    public navCtrl          : NavController, 
    public navParams        : NavParams,
    private alert           : Alert,
    private dataProvider    : DataProvider,
    private authProvider    : AuthProvider,
    private loadCtrl        : LoadingController) {

      this.email = this.navParams.get('email');
      this.password = this.navParams.get('password');
  }

  detailForm(){
    let loading = this.loadCtrl.create({content:'Cadastrando...', dismissOnPageChange: true});
    loading.present();
    this.authProvider.signUpUser(this.email, this.password)
      .then(user => {
        this.loggedUser = user;
        return this.loggedUser.sendEmailVerification();
      })
      .then(res => {
        let updatedProfile = {
          'displayName': `${this.name} ${this.lastname}`,
          'phoneNumber': this.phone,
          'birthday': this.birthday
        }
        return this.dataProvider.write(`users/${this.loggedUser.uid}`, updatedProfile);
      })
      .then(res => {
        loading.dismiss();
        this.navCtrl.push(HomePage);
      })
      .catch(err => {
        console.log(err);
      });
  }

  
}
