import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//providers
import {Alert} from '../../providers/alert';
import {AuthProvider} from '../../providers/auth-provider';

//pages 
import {SignUpPage} from '../sign-up/sign-up';
import {HomePage} from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  public email;
  public password;

  public isSendingForm: boolean = false;

  constructor(
    private navCtrl: NavController,
    private alert : Alert,
    private authProvider: AuthProvider
  ) {}

  goToSignUp(){
    this.navCtrl.push(SignUpPage);
  }

  goToHome(){
    this.navCtrl.push(HomePage);
  }

  loginForm(){
    this.isSendingForm = true;
    this.authProvider.logInUser(this.email, this.password)
      .then(res => {
        this.goToHome();
        this.isSendingForm = false;
      })
      .catch(err => {
        this.isSendingForm = false;
        this.alert.showBasicAlert('E-mail ou Senha incorreto','Se você esqueceu sua senha utilize a opção "Esqueceu a senha?" para recupera-la');
      });
  }

}
