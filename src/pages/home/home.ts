import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import {Http} from '@angular/http';
//providers
import {GetInformation} from '../../providers/get-information';
//pages
import {ChatPage} from '../chat/chat';
import {FriendsPage} from '../friends/friends';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  public escolha: string;
  public groupsMsgs;
  public peopleMsgs;

  constructor(
  public navCtrl: NavController, 
  private _http: Http,
  public loadingCtrl: LoadingController,
  public getInformation: GetInformation) {
    this.escolha = "grupos";
    this.fetchGroupMsgs();
  }

  goToChat(idChat){
    this.navCtrl.push(ChatPage,{idChat:idChat});
  }

  goToFriends(){
      this.navCtrl.push(FriendsPage);
  }

  fetchGroupMsgs(){
    let loading = this.loadingCtrl.create({
      content: 'Carregando Mensagens...'
    });
    
    loading.present();

    let success = (groupMsgs) => {
      this.groupsMsgs = groupMsgs.json();
      loading.dismiss();
    }

    let error = err => console.log(err);

    this.getInformation.retrieve('https://aluracar.herokuapp.com',success,error);
  }
}
