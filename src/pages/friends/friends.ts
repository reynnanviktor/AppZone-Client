import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
//providers
import {GetInformation} from '../../providers/get-information';

import firebase from 'firebase';

@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html'
})
export class FriendsPage {
  public queryText: string;
  public toggled: boolean;
  public recentlyTalk;
  constructor(public navCtrl: NavController,
    public navParams: NavParams, 
    private viewCtrl: ViewController,
    public getInformation: GetInformation,
    public loadingCtrl: LoadingController){  
      this.queryText = '';
      this.toggled = false;
      this.recentlyTalks();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendsPage');
  }

  searchContact(event){
    const ref = firebase.database().ref('users/')
    ref.on('value', (snap) => {
      const users = snap.val()
      const userSearchIds = Object.keys(users).filter((u) => users[u].displayName.toLowerCase().startsWith(event.target.value.toLowerCase()))
      // ids of users that were searched
    })
  }
  
  cancelSearch(){
    this.toggled = false;
    this.viewCtrl.showBackButton(true);
  }

  toggleSearch() {
      if(this.toggled == true){
        this.viewCtrl.showBackButton(true);
        this.toggled = false;
      }else{
        this.viewCtrl.showBackButton(false);
        this.toggled = true;
      }
    }

  recentlyTalks(){
    let loading = this.loadingCtrl.create({
      content: 'Carregando Mensagens...'
    });
    
    loading.present();

    let success = (recentlyTalk) => {
      this.recentlyTalk = recentlyTalk.json().results;
       loading.dismiss();
    }

    let error = err => console.log(err);

    this.getInformation.retrieve("https://randomuser.me/api/?results=3",success,error);
  }
    
  onlineFriends(){

  }

  allFriends(){

  }
}
