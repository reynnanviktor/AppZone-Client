import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//providers
import {Alert} from'../../providers/alert';
import {AuthProvider} from '../../providers/auth-provider';
//pages
import {DetailsSignUpPage} from '../details-sign-up/details-sign-up';
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})

export class SignUpPage {
  
  public email: string;
  public password: string;
  
  constructor(
    private navCtrl: NavController,
    private alert: Alert,
    private authProvider: AuthProvider) {

    }
  
  signUpForm(){
   if(this.email !== "" && this.password !== "")
    this.navCtrl.push(DetailsSignUpPage, {email: this.email, password: this.password});
   else
    this.alert.showBasicAlert("Campos vazios","Você precisa preencher todos os campos!");
  }

}
