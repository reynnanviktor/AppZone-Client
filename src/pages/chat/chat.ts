import { Component } from '@angular/core';
import {Http} from '@angular/http';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the Chat page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})

export class ChatPage {

  public idChat;
  public firstName;
  public lastName;
  public picture;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http) {     
    //idChat coming from click event on homePage definig who is the user that we are going to chat
    this.idChat = navParams.get('idChat');

    this.http
    .get('https://randomuser.me/api/')
    .map(res => res.json())    
    .toPromise()    
    .then(user => {
      this.firstName = user.results[0].name.first;
      this.lastName = user.results[0].name.last;
      this.picture = user.results[0].picture.medium;

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

}
