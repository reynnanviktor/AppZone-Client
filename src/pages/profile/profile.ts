import { Component } from '@angular/core';
import { ModalController, LoadingController } from 'ionic-angular';

import { ProfileEditModal } from '../profileEditModal/profileEditModal';

import {Alert} from '../../providers/alert';
import {DataProvider} from '../../providers/data-provider';
import {AuthProvider} from '../../providers/auth-provider';

import { User } from '../../models/user';
 
/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  //  script variables
  public loggedUser: firebase.User;
  public age : Number;
  public hasDatabaseInfo: boolean = false;

  //  models
  public user = {} as User;

  //  utils
  public loading;

  constructor(
  private alert : Alert, 
  private modalCtrl: ModalController,
  private authProvider: AuthProvider,
  private dataProvider: DataProvider,
  private loadingCtrl: LoadingController) {

    this.loading = loadingCtrl.create({content: "Carregando..."});
    this.loading.present();

    this.loggedUser = this.authProvider.currentUser();
    //addingBind
    this.dataProvider.read(`users/${this.loggedUser.uid}`)
      .then(this.updateUserInfo.bind(this))
      .catch(err => console.log(err));
  }

  openEdit(attribute){
    let editModal = this.modalCtrl.create(ProfileEditModal, { user: this.user });
    editModal.present();
  }

  updateUserInfo(snapshot): void{
    let doc                = snapshot.val();

    if(doc !== null){
      this.hasDatabaseInfo = true;
      this.user.displayName  = doc.displayName;
      this.user.phoneNumber  = doc.phoneNumber;
      this.user.status       = doc.status;
      this.user.birthday     = new Date(doc.birthday);
      this.user.instagram    = doc.instagram;
      this.user.snapchat     = doc.snapchat;
      this.user.twitter      = doc.twitter;
      this.age               = this.calculateAge(this.user.birthday);
    }
    this.user.uid            = this.loggedUser.uid;
    this.user.email          = this.loggedUser.email;

    this.loading.dismiss();
  }

  calculateAge(birthdate: Date): Number{
    let timeDiff = Math.abs(Date.now() - birthdate.getTime());
    return Math.floor((timeDiff / (1000 * 3600 * 24))/365);
  }

}
