import { Component } from '@angular/core';
import { Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import firebase from 'firebase';
import { FIREBASE_CONFIG } from './app.firebase.config';

import {LoginPage} from '../pages/login/login';
import {HomePage} from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any;

  constructor(
    private platform: Platform, 
    private statusBar: StatusBar, 
    private splashScreen: SplashScreen,
    private loadingCtrl: LoadingController
    ) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      firebase.initializeApp(FIREBASE_CONFIG);

      let loading = loadingCtrl.create({content: "Carregando..."});
      loading.present();

      //  listens to changes on if the user is logged in or not
      firebase.auth().onAuthStateChanged(user => {
        if(!user)
          this.rootPage = LoginPage;
        else
          this.rootPage = HomePage;
        loading.dismiss();
      });
      

    });
  }

  
}
