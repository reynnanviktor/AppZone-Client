import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//pages
import { HomePage } from '../pages/home/home';
import {ChatPage} from '../pages/chat/chat';
import {FindGroupsPage} from '../pages/find-groups/find-groups';
import {ProfilePage} from '../pages/profile/profile';
import {LoginPage} from '../pages/login/login';
import {SignUpPage} from '../pages/sign-up/sign-up';
import {DetailsSignUpPage} from '../pages/details-sign-up/details-sign-up';
import {FriendsPage} from '../pages/friends/friends';
//components
import {AppHeaderComponent} from '../components/app-header/app-header';
import {GroupChatComponent} from '../components/group-chat/group-chat';
import {UserChatComponent} from '../components/user-chat/user-chat';
import {ChatBubbleComponent} from '../components/chat-bubble/chat-bubble';
import {GroupChatBubbleComponent} from '../components/group-chat-bubble/group-chat-bubble';
import {EditSliderComponent} from '../components/edit-slider/edit-slider';
import {GroupCardComponent} from '../components/group-card/group-card';
import {FriendFaceComponent} from '../components/friend-face/friend-face';
//providers
import {PostInformation} from '../providers/post-information';
import {GetInformation} from '../providers/get-information';
import {PatchInformation} from '../providers/patch-information';
import {Alert} from '../providers/alert';
import {TokenManager} from '../providers/token-manager';
import {AuthProvider} from '../providers/auth-provider';
import {DataProvider} from '../providers/data-provider';
//
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//modals
import { ProfileEditModal } from '../pages/profileEditModal/profileEditModal';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChatPage,
    ProfilePage,
    FindGroupsPage,
    LoginPage,
    SignUpPage,
    DetailsSignUpPage,
    FriendsPage,
    AppHeaderComponent,
    GroupChatComponent,
    UserChatComponent,
    ChatBubbleComponent,
    GroupChatBubbleComponent,
	  EditSliderComponent,
    GroupCardComponent,
    ProfileEditModal,
    FriendFaceComponent
    ],
  imports: [
    IonicModule.forRoot(MyApp,{
       tabsPlacement: 'top',
       backButtonText: ''
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChatPage,
    ProfilePage,
    FindGroupsPage,
    LoginPage,
    SignUpPage,
    DetailsSignUpPage,
    FriendsPage,
    ProfileEditModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PostInformation,
    PatchInformation,
    GetInformation,
    Alert,
    TokenManager,
    AuthProvider,
    DataProvider
]
})
export class AppModule {}
