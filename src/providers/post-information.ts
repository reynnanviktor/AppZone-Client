import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostInformation {
  private url: string = "https://appzone.herokuapp.com/";

  constructor(public http: Http) {
    console.log('PostInformation Provider');
  }

  send(endPoint,postData,success,error,contentType='application/json'){
    let headers = new Headers();
    headers.append('Content-Type',contentType);

    this.http
    .post(this.url+endPoint,JSON.stringify(postData),{headers: headers})
    .subscribe(success, error); //callback receive data(json) as parameter
  }

}
