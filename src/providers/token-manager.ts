import { Injectable } from '@angular/core';

@Injectable()
export class TokenManager {
  private tokenKey:string;

  constructor() {
    this.tokenKey = 'app_token';
  }

  public store(content:string) {
        localStorage.setItem(this.tokenKey, content);
  }

  public retrieve() {
      let storedToken:string = localStorage.getItem(this.tokenKey);
      if(!storedToken) throw 'no token found';
      return storedToken;
  }

  public remove(){
    localStorage.removeItem(this.tokenKey);
  }
  
}
