import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class Alert {

  constructor(public alertCtrl: AlertController) {
    console.log('Hello Alert Provider');
  }
  
  showBasicAlert(textTitle,textSubTitle) {
    let alert = this.alertCtrl.create({
      title: textTitle,
      subTitle: textSubTitle,
      buttons: ['OK']
    });
    alert.present();
  }
}
