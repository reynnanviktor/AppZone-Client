import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GetInformation {

  //private url: string = "https://appzone.herokuapp.com/";

  constructor(public http: Http) {
    console.log('GetInformation Provider');
  }

  /* FAZER MUDANÇA */
  //auxUrl como parametro só por um tempo, após a url da API estiver correta
  //depois disso usar this.url + endpoint(como parametro) 
  retrieve(auxUrl,success,error){
    this.http
    .get(auxUrl)
    .subscribe(success, error);
  }
}
