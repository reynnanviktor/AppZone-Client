import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PatchInformation {

  private url: string = "https://appzone.herokuapp.com/";

  constructor(private _http: Http) {
    console.log('Hello PatchInformation Provider');
  }

  modify(endPoint,patchData,success,error,token,contentType='application/json'){
    let headers = new Headers();
    headers.append('Content-Type',contentType);
    headers.append('x-access-token',token);

    this._http
    .patch(this.url+endPoint, JSON.stringify(patchData), {headers: headers})
    .subscribe(success, error);
  }

}
