import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import firebase from 'firebase';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataProvider {

  constructor(public http: Http) {

  }

  write(ref: string, obj){
    return firebase.database().ref(ref).set(obj);
  } 

  read(ref: string): firebase.Promise<any> {
    return firebase.database().ref(ref).once('value');
  }
  
  update(ref: string, obj: any) {
    return firebase.database().ref(ref).update(obj);
  }

}
