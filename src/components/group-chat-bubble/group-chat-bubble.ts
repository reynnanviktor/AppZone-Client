import { Component, Input } from '@angular/core';

/*
  Generated class for the GroupChatBubble component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'group-chat-bubble',
  templateUrl: 'group-chat-bubble.html'
})

export class GroupChatBubbleComponent {

  @Input() position;
  @Input() content;
  @Input() senderName;

  constructor() {
    console.log("HEllo Group Chat BUbble");
  }


}
