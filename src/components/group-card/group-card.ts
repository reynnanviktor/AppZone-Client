import { Component, Input } from '@angular/core';

/*
  Generated class for the GroupCard component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'group-card',
  templateUrl: 'group-card.html'
})
export class GroupCardComponent {
  @Input() img;
  @Input() name;
  @Input() location;
  @Input() desc;
  @Input() km;
  @Input() amount;
  
  constructor() {
    console.log('Hello GroupCard Component');
  }

}
