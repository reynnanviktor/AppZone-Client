import { Component, Input } from '@angular/core';

/*
  Generated class for the UserChat component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/

@Component({
  selector: 'user-chat',
  templateUrl: 'user-chat.html'
})
export class UserChatComponent {
  
  @Input() userName;
  @Input() lastMsg;
  @Input() lastMsgTime;
  @Input() unreadMsg;
  
  constructor() {
    console.log('Hello UserChat Component');
  }

  returnInt(unreadMsgAux){
      return parseInt(unreadMsgAux);
  }
  
}