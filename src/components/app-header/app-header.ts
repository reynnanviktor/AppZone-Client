import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//pages
import {FindGroupsPage} from '../../pages/find-groups/find-groups';
import {ProfilePage} from '../../pages/profile/profile';

@Component({
  selector: 'app-header',
  templateUrl: 'app-header.html'
})
export class AppHeaderComponent {

  constructor(public navCtrl: NavController) {
    console.log('Hello AppHeader Component');
  }

  goToFindGroups(){
    this.navCtrl.push(FindGroupsPage);
  }

  goToProfile(){
    this.navCtrl.push(ProfilePage);
  }

}
