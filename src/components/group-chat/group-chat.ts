import { Component, Input } from '@angular/core';

/*
  Generated class for the GroupChat component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'group-chat',
  templateUrl: 'group-chat.html'
})
export class GroupChatComponent {
  //@Input() imgUser;
  @Input() groupName;
  @Input() lastMsg;
  @Input() lastMsgName;
  @Input() lastMsgTime;
  constructor() {
    console.log('Hello GroupChat Component');
  }
  swipeEvent(e) {
    console.log(e);
  }
}
