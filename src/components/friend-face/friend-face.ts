import { Component, Input } from '@angular/core';
import {ActionSheetController, Platform } from 'ionic-angular'
/*
  Generated class for the FriendFace component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'friend-face',
  templateUrl: 'friend-face.html'
})
export class FriendFaceComponent {

  @Input() name;
  @Input() status;
  @Input() img;
  constructor( public actionSheetCtrl: ActionSheetController,  public platform: Platform) {
    console.log('Hello FriendFace Component');
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opções',
      buttons: [
        {
          text: 'Enviar Mensagem',
          icon: !this.platform.is('ios') ? 'chatboxes' : null,
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Deletar',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            console.log('Archive clicked');
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  
}
